package br.com.unidesc.entidades.interfaces;

import br.com.unidesc.entidades.Aluno;

import java.util.List;

public interface IcontrollerAluno {
    void criar(Aluno aluno);

    void atualizar(Aluno aluno);

    void deletar(Aluno aluno);

//    String recuperar (Aluno String);

    List<Aluno> listar(Aluno aluno);
}
package br.com.unidesc.rest;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.unidesc.controller.AlunoController;
import br.com.unidesc.entidade.Aluno;


@Path("/aluno")
public class AlunoResource {
    
    AlunoController controller = new AlunoController();
    private static Logger logger = Logger.getLogger(AlunoResource.class.getName());
    
    @Path("/servicestatus")
    @GET 
    @Produces("text/plain")
    public String getIt() {
        return "Hi there!";
    }
    
    
    @Path("/listar")
    @GET 
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarAlunos() {
    	Aluno aluno = new Aluno();
    	aluno.setNome("J");
        List<Aluno> output = controller.listarAlunos(aluno); 
        GenericEntity<List<Aluno>> list = new GenericEntity<List<Aluno>>(output) {
        };
         
        return Response.status(200).entity(list).build();
    }
    
    @Path("/listar-alunos")
    @GET 
    @Produces(MediaType.APPLICATION_JSON)
    public List<Aluno> listar() {
    	Aluno aluno = new Aluno();
    	aluno.setNome("J");
    	
        List<Aluno> output = controller.listarAlunos(aluno); 
         
        return output;
    }
    
    @Path("/criar-novo-aluno")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Aluno addAluno(Aluno aluno) {
    	
    	logger.info("curso aluno:" + aluno.getCurso());
        aluno = controller.criarAluno(aluno); 
         
        return aluno;
    }
    
}

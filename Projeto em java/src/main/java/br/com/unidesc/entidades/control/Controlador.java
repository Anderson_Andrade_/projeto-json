package main.java.br.com.unidesc.entidades.control;
//imports
    import br.com.unidesc.entidades.Aluno;

    import java.io.BufferedWriter;
    import java.io.File;
    import java.io.FileWriter;

    //interface
    import br.com.unidesc.entidades.interfaces.IcontrollerAluno;
    import java.io.IOException;
    import java.util.ArrayList;
    import java.util.Calendar;
    import java.util.List;

    //json
    import org.apache.commons.io.FileUtils;
    import org.json.JSONException;
    import org.json.JSONObject;

public class Controlador implements IcontrollerAluno{
    private static final String CAMINHO_DO_ARQUIVO = "Lista de alunos.json";

    private String gerarMatricula (String ultimaMatriculaCadastrada){
        //Gerar matricula
//        Calendar hoje = Calendar.getInstance();
//        int ano = hoje.get(Calendar.YEAR);
//        int mes = hoje.get(Calendar.MONTH);
//        String semestre;
//        int seguencial = 1;
//        String complementarDaMatricula = String.format("%05d",seguencial);
//
//        if(mes < 6){
//            semestre = "01";
//        }
//        else{
//            semestre = "02";
//        }
//        String codigoDaMatricula = ano + semestre + complementarDaMatricula;
//        return codigoDaMatricula;
//--------------------------------------------------------------------------------------
        String matricula = null;
        Integer ano = Calendar.getInstance().get(Calendar.YEAR);
        String semestre = Calendar.getInstance().get(Calendar.MONTH) > 5 ? "02" : "01";
        String sequencial = null;
//        Calendar c1 = Calendar.getInstance();
//
//        Interger ano = c1.get(Calendar.YEAR);
//        int mes = c1.get(Calendar.MONTH);
//        String sequencial = null;
//        String semestre;
//
//        if (mes > 5) {
//            semestre = "02";
//
//        } else {
//            semestre = "01";
//        }

        if (ultimaMatriculaCadastrada == null) {
            sequencial = String.format("%05d", 1);
        }
        else {
            Integer numeroSequencial = Integer.parseInt(ultimaMatriculaCadastrada.substring(6)) + 1;
            sequencial = String.format("%05d", numeroSequencial);
        }

        return matricula = ano.toString() + semestre + sequencial;
    }

    private String recuperarMatricula() {
        String matricula = null;

        try
        {
            File file = new File(CAMINHO_DO_ARQUIVO);
            List<String> lines = FileUtils.readLines(file, "UTF-8");

            for (String object : lines)
            {
                JSONObject toJsonObject = new JSONObject(object.toString());
                matricula = toJsonObject.getString("matricula");
            }
        }

        catch (JSONException | IOException e)
        {
            e.printStackTrace();
        }
        return matricula;

    }


//    CRUD

    @Override
    public void criar(Aluno aluno) {
        File file = new File(CAMINHO_DO_ARQUIVO);
        JSONObject jsonObject = new JSONObject(aluno);
        FileWriter writerFile = null;


        if (!file.exists()){
            try {
                writerFile = new FileWriter(CAMINHO_DO_ARQUIVO);
                aluno.setMatricula(gerarMatricula(null));
                jsonObject = new JSONObject(aluno);


                BufferedWriter buffer = new BufferedWriter(writerFile);
                buffer.write(jsonObject.toString());
                buffer.newLine();
                buffer.close();
            } catch (IOException e){
                e.printStackTrace();
            }
        }
        else{
            try {
                writerFile = new FileWriter(CAMINHO_DO_ARQUIVO, true);
                aluno.setMatricula(gerarMatricula(recuperarMatricula()));
                jsonObject = new JSONObject(aluno);
                BufferedWriter buffer = new BufferedWriter(writerFile);
                buffer.write(jsonObject.toString());
                buffer.newLine();
                buffer.close();
            } catch (IOException e){
                e.printStackTrace();
            }

        }
    }
    @Override
    public void atualizar(Aluno aluno){
        JSONObject objectToJson = new JSONObject(aluno);
        File file = new File(CAMINHO_DO_ARQUIVO);
        try {
            List<String > lines = FileUtils.readLines(file, "UTF8");
            int index = -1;
            for (String object : lines) {
                JSONObject toJsonObject = new JSONObject(object.toString());
                if (aluno.getMatricula().equals(toJsonObject.getString("matricula"))) {
                    index = lines.indexOf(object);
                }
            }
            if (index > -1) {
                lines.set(index, objectToJson.toString());
                FileUtils.writeLines(file, lines);
            }
        }
        catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deletar(Aluno aluno) {
        JSONObject objectToJson = new JSONObject(aluno);
        File file = new File(CAMINHO_DO_ARQUIVO);
        try
        {
            List<String > lines = FileUtils.readLines(file, "UTF8");
            int index = 0;
            for (String object : lines)
            {
                JSONObject toJsonObject = new JSONObject(object.toString());
                if (aluno.getMatricula().equals(toJsonObject.getString("matricula")))
                {
                    index = lines.indexOf(object);
                }
            }
            lines.remove(index);
            FileUtils.writeLines(file, lines);
        }
        catch (IOException | JSONException e)
        {
            e.printStackTrace();
        }

    }
//    @Override
//    public Aluno recuperar(Aluno String) {
//        JSONObject objectJson = new JSONObject();
//        List<Aluno> listAluno = new ArrayList<>();
//        File file = new File(CAMINHO_DO_ARQUIVO);
//
//        try{
//            List<String> line = FileUtils.readLines(file, "utf8");
//            int index = -1;
//
//            for(String object : line ){
//                JSONObject jsonObject = new JSONObject(objectJson.toString());
//
//                if(jsonObject.getString("nome").contains(aluno.getNome())){
//                    Aluno list = new Aluno();
//                    list.setNome(jsonObject.getString("Nome"));
//                    list.setMatricula(jsonObject.getString("Matricula"));
//                    list.setNascimento(jsonObject.getString("Nascimento"));
//                    list.setEmail(jsonObject.getString("Email"));
//                    list.setCurso(jsonObject.getString("Curso"));
//                    list.setCpf(jsonObject.getString("Cpf"));
//                    list.setNumero(jsonObject.getString("Numero"));
//                }
//            }
//        } catch (IOException | JSONException e) {
//            e.printStackTrace();
//        }
//        return listAluno;
//
//    }
    @Override
    public List<Aluno> listar(Aluno aluno) {
        JSONObject objectJson = new JSONObject();
        List<Aluno> listAluno = new ArrayList<>();
        File file = new File(CAMINHO_DO_ARQUIVO);

        try{
            List<String> line = FileUtils.readLines(file, "utf8");
            int index = -1;

            for(String object : line ){
                JSONObject jsonObject = new JSONObject(object.toString());

                if(jsonObject.getString("nome").contains(aluno.getNome())){
                    Aluno list = new Aluno();
                    list.setNome(jsonObject.getString("nome"));
//                    list.setMatricula(jsonObject.getString("Matricula"));
//                    list.setNascimento(jsonObject.getString("Nascimento"));
//                    list.setEmail(jsonObject.getString("email"));
//                    list.setCurso(jsonObject.getString("Curso"));
//                    list.setCpf(jsonObject.getString("Cpf"));
//                    list.setNumero(jsonObject.getString("Numero"));
                    listAluno.add(list);
                }
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return listAluno;
    }

    public static void main(String[] args) {
        Aluno aluno = new Aluno();
        aluno.setNome("testes");
        aluno.setNascimento("456546");
        aluno.setCpf("asdasdasd");
        aluno.setCurso("adasdasdasd");
        aluno.setEmail("asadasdasd");

        Controlador controlador = new Controlador();
        controlador.criar(aluno);
        controlador.criar(aluno);

        aluno.setMatricula("20180200001");
        aluno.setNome("zé");
        controlador.atualizar(aluno);
        aluno.setNome("t");

        System.out.println(controlador.listar(aluno).toString());
    }
}